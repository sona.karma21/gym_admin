(function () {
    'use strict';
    angular.module('app')
      .controller('partnerCtrl', partnerCtrl);

    partnerCtrl.$inject = ['$scope', 'aPIInterFace', '$uibModal', '$uibModalInstance', 'partnerId'];

    function partnerCtrl($scope, aPIInterFace, $uibModal, $uibModalInstance, partnerId) {
        var vm = this;
        vm.GetF_P = GetF_P;
        vm.initF_P = initF_P;
        //vm.cancelPartner = cancelPartner;
        function initF_P() {
            vm.GetF_P(partnerId);
            //console.log(partnerId);
        }
        $scope.cancelPartner = function() {
            $uibModalInstance.dismiss('cancel');
        };
        function GetF_P(id){        
            aPIInterFace.doServiceCall('Get', 'TblFacilitiesById', {params:{id:id}}).then(function (response) {
                if (response.Success) {
                    vm.onlyFacilPartNameData = response.Result.parteners;
                }
                else {
                    LoaderStop();
                }
            });
        } 
    }
})();