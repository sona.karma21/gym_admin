(function(){
	'use strict';
	angular.module('app')
	.controller('categoryCtrl', categoryCtrl)

	function categoryCtrl($scope, $state, $uibModal, aPIInterFace, $location, $timeout, dialogs, AuthSrv,Config,alertService) {
		var vm = this;
		$scope.vm = vm;
		vm.allCategories = [];
		vm.parentId = 0;
		vm.selectedCat = [];
		$scope.images = [];
		vm.newCategoryId = 0;
		vm.selectFile = selectFile;
		vm.tempImgs = [];
		vm.newImage= [];
		$scope.selectedCat ={
			categoryId:0,
			categoryCss:{}
		};
		vm.superCategory = [];

		$scope.viewby = 10;
        $scope.currentPage= 1;
        $scope.maxSize = 5


		var modal = $uibModal;

		vm.init = function()
		{
			var id= $location.url();
			vm.parentId = parseInt(id.split('/').pop()) || 0;
			vm.getCategories();
		}
		//get Category call
		vm.getCategories = function(){
			aPIInterFace.doServiceCall('GET', 'getadmincategories', {}).then(function(response){
				vm.allCategories = response.Result;
					var i = 0
					_.forEach(vm.allCategories, function(cat){
						if(cat.parentId != 0){
							vm.selectedCat[i] = cat;
							i++;
						}

						
					})
					vm.categories = vm.filterCategory(response.Result);
					vm.superCategory = vm.superCategoryFun(response.Result);


					_.map(vm.categories,function(data){
						try{
							data.categoryCss = JSON.parse(data.categoryCss);	
						}
						catch(err) {
						    data.categoryCss = {};
						}

					});
					$scope.totalItemsUp = vm.categories.length;
					$scope.itemsPerPage = $scope.viewby;
		
				});
			}

			vm.filterCategory = function(data){
				var result = _.filter(data, function(result){
					return result.parentId == vm.parentId;
				});
				return result;
			}

			vm.superCategoryFun = function(data){
				var result = _.filter(data, function(result){
					return result.parentId == 0;
				});
				return result;
			}

			$scope.deleteFile = function(image, index){
				image.imageUrl.splice(index, 1);
			}

			
 			// code for select image in edit and add category
 			function selectFile(selectedCat){
 				$scope.selectedCat = selectedCat;
 				$("#file").click();
 			}

 			$scope.uploadImage = function (input) {
 				if (input.files && input.files[0]) {
					var data = new FormData();
		            data.append("image1", input.files[0]);
		            
					$scope.selectedCat.categoryCss.imageUrl= $scope.selectedCat.categoryCss.imageUrl || [];
		            AuthSrv.saveImages(data).then(function(res){
		            	$scope.selectedCat.categoryCss.imageUrl.push(Config.api_rootWebImage + res.Result);
		            	console.log($scope.selectedCat);
		            },function(error){
		            	vm.errorMessage = error.Message;
		            	alertService.add('success', '!Error while uploading , Please try again');
		            });

 				}
	 		}

		// open modal for add and edit the category
		vm.openAdd = function(cat){
			if(cat){
				$scope.selectedCat = cat;
			}
			else
			{
				$scope.selectedCat ={
					categoryId:0,
					categoryCss:{}
				}	
			}

			$scope.parent = vm.superCategory;
			
			$scope.modalIns = modal.open({
				animation: true,
				templateUrl: 'module/admin/category/add_editCategory.html',
				scope: $scope,
				backdrop: 'static',
				keyboard: false
			});

			function status(cat){
			}
			


			$scope.NewCategory = function(selectedCat){
	            var constructCustomer = {
	                "categoryId": selectedCat.categoryId || 0,
	                "parentId": selectedCat.parentId || 0,
	                "categoryName": selectedCat.categoryName,
	                "categoryCss": JSON.stringify(selectedCat.categoryCss),
	                "categoryDescription": selectedCat.categoryDescription
	            }
	            AuthSrv.submitNewCategory(constructCustomer).then(function(response){
	                if(response.data.Success){
	                	vm.getCategories();
	                	$scope.cancel();
	                	alertService.add('success', 'Data saved successfully');
	                    
	                }else{
	                	$scope.cancel();
	                	alertService.add('danger', response.data.Message);
	                }
	            },function(error){
	            	$scope.cancel();
	            	alertService.add('danger', 'Please enter a valid Information');
	            });
	        }

			$scope.cancel = function() {
				$scope.modalIns.dismiss('cancel');
			};
		};

		

				

		// open modal for delete the category
		vm.confirmDelete = function(cat){
			console.log(cat);
			var msg = 'Are you sure, you want to delete?';
			var dlg = dialogs.confirm('Confirmation', msg,{size:'sm'});
			dlg.result.then(function (btn) {
				var params = {
					"isDelete": true,
					"isActive": true,
					"id": cat.categoryId || 0,
					"type": 4
				}

				if(cat.parentId == 0){

					var filterData = _.filter(vm.allCategories ,function(data){
						return data.parentId == cat.categoryId
					});

					_.map(filterData,function(scat){

						params.id= scat.categoryId;
						deleteCategory(params)
					});
				}		

				params.id= cat.categoryId;
				deleteCategory(params)

			});
		}

		function deleteCategory(params){
			AuthSrv.deleteCategory(params).then(function(response){
                if(response.Success){
                	vm.getCategories();
                    alertService.add('success', 'successfully');
                }else{
                    alertService.add('danger', 'Please try again');
                }
            },function(error){
                alertService.add('danger', 'Please try again');
            });
		}


		// open modal for delete the category
		vm.confirmActiveInActive = function(cat, active){
			var msg = 'Are you sure, you want to '+ ((active)?'In Active': 'Deactive') +'?';
			var dlg = dialogs.confirm('Confirmation', msg,{size:'sm'});
			dlg.result.then(function (btn) {
				if(btn == 'yes'){
					var params = {
						"isActive": active,
						"id": cat.categoryId,
						"type": 4
					}

					if(cat.parentId == 0){

						var filterData = _.filter(vm.allCategories ,function(data){
							return data.parentId == cat.categoryId
						});

						_.map(filterData,function(scat){

							params.id= scat.categoryId;
							deleteCategory(params);
						});
					}		

					params.id= cat.categoryId;
					deleteCategory(params);
				}else{
					alertService.add('danger', 'Please try again');
				}
				

			});
		}





	};
})();