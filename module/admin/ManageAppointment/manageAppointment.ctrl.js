(function () {
    'use strict';
    angular.module('app')
      .controller('manageAppointmenCtrl', manageAppointmenCtrl);

    function manageAppointmenCtrl($scope, $http, UserSrv, aPIInterFace, $uibModal,alertService,dialogs, uibDateParser, $filter) {
        var currentUser = JSON.parse(localStorage.getItem('app-user'));
        var vm = {};
        $scope.vm = vm;
        vm.GetAppointments = GetAppointments;
        vm.newAppointments = newAppointments;
        vm.initAppoint = initAppoint;
        vm.createAdminApointment = createAdminApointment;
        vm.adminAppotEdit = adminAppotEdit;
        vm.Delete = Delete;
        vm.GetFacilities=GetFacilities;
        vm.GetPartners=GetPartners;
        vm.adminAppotUsers = adminAppotUsers;
        vm.getAppFacility=getAppFacility;
        vm.userChange = userChange;

                vm.housrList = ["00:00", "00:15", "00:30", "00:45", "01:00", "01:15", "01:30", "01:45",
  "02:00", "02:15", "02:30", "02:45", "03:00", "03:15", "03:30", "03:45", "04:00", "04:15", "04:30", "04:45",
  "05:00", "05:15", "05:30", "05:45", "06:00", "06:15", "06:30", "06:45", "07:00", "07:15", "07:30", "07:45", "08:00", "08:15", "08:30", "08:45", "09:00",
  "09:15", "09:30", "09:45",
  "10:00", "10:15", "10:30", "10:45", "11:00", "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "13:00", "13:15", "13:30", "13:45", "14:00",
 "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "16:15", "16:30", "16:45", "17:00", "17:15", "17:30", "17:45",
  "18:00", "18:15", "18:30", "18:45", "19:00", "19:15", "19:30", "19:45", "20:00", "20:15", "20:30", "20:45",
  "21:00", "21:15", "21:30", "21:45", "22:00", "22:15", "22:30", "22:45", "23:00", "23:15", "23:30", "23:45"]

        vm.isUsers = false;
        vm.isUserName = false;
        vm.popup1 = {
            opened: false
        };
        // vm.dateOptions = {
        //     formatYear: 'yy',
        //     maxDate: new Date(2020, 5, 22),
        //     minDate: new Date(),
        //     startingDay: 1
        // };
        vm.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        vm.format = vm.formats[0];
        vm.altInputFormats = ['M!/d!/yyyy'];
        $scope.open1 = function() {
            vm.popup1.opened = true;
        };

        vm.options = {
            hstep: 1,
            mstep: 1
        };

        $scope.changed = function () {
            debugger;
            vm.appointmentsModel.AppointmentTime = $filter('date')(vm.mytime, 'HH:mm');
        };

        //all appointee list start
        function GetAppointments(){
        	aPIInterFace.doServiceCall('Get', 'TblAppointments', null).then(function (response) {
                if (response.Success) {
                    vm.AppointmentsList = response.Result;
                    //console.log(vm.AppointmentsList);
                }
                else {
                }
            });
        }
        //all appointee list end

        // appointee new form
        function newAppointments(){

                vm.appointmentsModel = {
                  "Id": 0,
                  "fkPartnerId": "0",
                  "fkFacilityId": "0",
                  "fkUserId": "0",
                  "UserName": "",
                  "AppointmentDate": "",
                  "AppointmentTime":"",
                  "AppointeeName":"",
                  "GeneralNotes":"",
                  "CreatedBy":currentUser.UserId,
                  "CreatedByUserType":currentUser.UserType

                }
        }

        function userChange(user){
            console.log(user);
            if(user == '0'){
                vm.isUsers = false;
                vm.isUserName = false;

            }else{
                vm.isUsers = true;
                vm.isUserName = true;

            }
        }

        //create new admin appointment start
        function createAdminApointment(){
            vm.myDataAdminAppoint.submitted = true;

            if(vm.myDataAdminAppoint.$valid && (vm.isUserName == true || vm.appointmentsModel.UserName)){
                LoaderStart();
                vm.isUserName == false;
                vm.isUsers = false;
                aPIInterFace.doServiceCall('Post', 'TblAppointments', vm.appointmentsModel).then(function (response) {
                    if (response.Success) {
                        vm.AppointmentsList = response.Result;
                        vm.newAppointments();
                        $('#adminAppotClose').click();
                        LoaderStop();
                        vm.GetAppointments();
                    }
                    else {
                      console.log('error : ' +response);
                      LoaderStop();
                    }
                });

            }

        }
        //create new admin appointment end

        //user start
        function adminAppotUsers(id){
            if(id != 0){
                LoaderStart();
                aPIInterFace.doServiceCall('Get', 'TblEndUsers', {params:{FacilityId:id}}).then(function (response) {
                    if (response.Success) {
                        vm.appointmentsUser = response.Result;
                        LoaderStop();
                    }
                    else {
                        LoaderStop();
                    }
                });
            }
        }
        //user end

        //edit admin appointment start
        function adminAppotEdit(id){
            console.log(id);
            LoaderStart();
            aPIInterFace.doServiceCall('Get', 'TblAppointmentsById',{params:{id:id}}).then(function (response) {
                if (response.Success) {

                    adminAppotUsers(response.Result.fkFacilityId)
                    vm.isUserName = (!response.Result.fkUserId)? false:true;
                    vm.appointmentsModel = {
                                  "Id": response.Result.Id,
                                  "fkPartnerId": response.Result.fkPartnerId.toString(),
                                  "fkFacilityId": response.Result.fkFacilityId.toString(),
                                  "fkUserId": response.Result.fkUserId.toString(),
                                  "UserName": response.Result.UserName,
                                  "AppointmentDate": new Date(response.Result.AppointmentDate),
                                  "AppointmentTime":response.Result.AppointmentTime,
                                  "AppointeeName":response.Result.AppointeeName,
                                  "GeneralNotes":response.Result.GeneralNotes,
                                  "CreatedBy":currentUser.UserId,
                                  "CreatedByUserType":currentUser.UserType

                        }
                    LoaderStop();
                }
                else {
                    LoaderStop();
                }
            });
        }
        //edit admin appointment end

          //Delete Language start
        function Delete(id) {

                //var msg = (job.jobDescription) ? 'More : ' + job.jobDescription + ' <br/> <hr/>' : '';
                var msg = 'Are you sure, you want to delete?';
                var dlg = dialogs.confirm('Confirmation', msg,{size:'sm'});

                dlg.result.then(function (btn) {

                    LoaderStart();

                        var objReq={
                            IsDelete:true,
                            IsActive:false,
                            Id:id,
                            UserType:5
                        }

                        aPIInterFace.doServiceCall('Post', 'DeleteActive', objReq).then(function (response) {
                            //console.log(response);
                            if (response.Success) {
                              alertService.add('success', response.Message);
                                LoaderStop();
                                $('#app_'+id).remove();
                            } else {
                               alertService.add('danger', response.Message);
                                LoaderStop();
                            }


                        });



                }, function (btn) {

                });

        }
        //Delete Language end

        //partner's facility start
        function getAppFacility(){
        		aPIInterFace.doServiceCall('Get', 'GetFacilitiesForPartners', {params:{PartnerId:vm.appointmentsModel.fkPartnerId,Email:""}}).then(function (response) {
	                if (response.Success) {
	                    vm.facilitiesList = response.Result;
	                }
	                else {
	                }
	            });

        }
        //partner's facility end


        //--------GetFacilities List-----------
        function GetFacilities() {
            aPIInterFace.doServiceCall('Get', 'TblFacilities', null).then(function (response) {
                if (response.Success) {
                    vm.facilitiesList = response.Result;
                }
                else {
                }
            });

        }
 /*       partner list start*/
        function GetPartners(){
         aPIInterFace.doServiceCall('Get', 'TblPartners', null).then(function (response) {
                if (response.Success) {
                    vm.partnersList = response.Result;
                }
                else {
                }
            });
        }
/*        partner list end
*/

        function initAppoint() {
           vm.GetAppointments();
            vm.newAppointments();
            vm.GetPartners();
            vm.GetFacilities();
        }

    }
})();
