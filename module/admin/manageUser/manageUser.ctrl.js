(function(){
	'use strict';
	angular.module('app')
	.controller('customerCtrl', customerCtrl)

	function customerCtrl($scope, $location, aPIInterFace, $uibModal, dialogs,UserSrv, alertService)
	 {
		var vm = this;
		$scope.vm = vm;
		var modal = $uibModal;
		vm.isShow = false;
		vm.type = 0;


		$scope.viewby = 10;
        $scope.currentPage= 1;
        $scope.maxSize = 5

		vm.init = function()
		{	
			var urlType= $location.url().split('/').pop();
			vm.type = (urlType == 'manageUser')?2 :(urlType == 'provider')?3 : (urlType == 'category')?4 : 5 ;
			vm.getUsers();

			UserSrv.get().then(function(res){
				vm.user = res;
			})
		}

		vm.actionStatus = function(obj){
			var params = {
				"isDelete": obj.isDelete || false,
				"isActive": obj.isActive || false,
				"id": obj.userId,
				"type": vm.type
			}
			return aPIInterFace.doServiceCall('POST', 'usersAction', params);
		}

		vm.getUsers = function(){
			var params= {
				type : vm.type
			}
 			aPIInterFace.doServiceCall('GET', 'usersType', {params}).then(function(response){
 				vm.allDataByType = response.Result;
 				$scope.totalItemsUp = vm.allDataByType.length;
				$scope.itemsPerPage = $scope.viewby;
 			});
 			
 		}

		vm.confirmDelete = function(user){
			var obj={
				"isDelete":true,
				"isActive": true,
				"userId" : user.userId,
				"type" : vm.type
			}
			var msg = 'Are you sure, you want to delete?';
			vm.confirmActions(obj,msg)
		};

		vm.confirmActive = function(user, isActive){ 
			var obj={
				"isActive":isActive,
				"type" : vm.type,
				"userId": user.userId
			}
			if(isActive){
				var msg = 'Are you sure, you want to Active?';
			}else{
				var msg = 'Are you sure, you want to Deactive?';
			}
			
			vm.confirmActions(obj,msg)
		};

		vm.confirmActions = function(obj,msg)
		{
			var msg = msg;
			var dlg = dialogs.confirm('Confirmation', msg,{size:'sm'});
				dlg.result.then(function (btn) {

				vm.actionStatus(obj).then(function(response){
					if (response.Success)
					{
						vm.getUsers();
						alertService.add('success', response.Message);
						LoaderStop();
					} else
					{
						alertService.add('danger', response.Message);
						LoaderStop();
					}
				})
			});
		};


		vm.openModal = function(cat)
		{
		  {
		    $scope.modalIns = modal.open({
		      animation: true,
		      templateUrl: 'module/admin/category/add_editCategory.html',
		      size: 'sm',
		      scope: $scope,
		      backdrop: 'static',
		      keyboard: false
		    });
		    $scope.modalIns.result.then(function () {
		    });
		  }
		}
	}
})();
