(function () {
  'use strict';
  angular.module('app')
  .config(configure);

  function configure($stateProvider) {
    $stateProvider
    .state('admin.dashboard', {
      url: '/dashboard',
      diplayname:'Dashboard',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'module/admin/dashboard/dashboard.html',
          controller: 'DashboardCtrl'
        }
      }
    })
    .state('admin.category', {
      url: '/category/',
      diplayname:'Category',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'module/admin/category/category.html',
          controller: 'categoryCtrl'
        }
      }
    })
    .state('admin.subCategory', {
      url: '/subCategory/:id',
      diplayname:'Sub Category',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'module/admin/category/subCategory.html',
          controller: 'categoryCtrl'
        }
      }
    })
    .state('admin.manageUser', {
      url: '/manageUser',
      diplayname:'User',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'module/admin/manageUser/manageUser.html',
          controller: 'customerCtrl'
        }
      }
    })
    .state('admin.provider', {
      url: '/provider',
      diplayname:'Provider',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'module/admin/manageUser/manageUser.html',
          controller: 'customerCtrl'
        }
      }
    })
  }
})();
