(function(){
  'use strict';
  angular.module('app')
    .controller('LoginCtrl', LoginCtrl);

  function LoginCtrl($scope, $state, StorageUtils, aPIInterFace,UserSrv){
    var vm = {};
    $scope.vm = vm;

    vm.error = null;
    vm.loding = false;
    vm.credentials = {login: '', password: ''};
    vm.login = login;

    function login(credentials){
      debugger;
      vm.error = null;
      vm.loading = true;

      var req = {
        "emailId": credentials.userName,
        "password": credentials.password,
        "isLogin": true
      }
      aPIInterFace.doServiceCall('Post', 'login', req).then(function (response) {
        LoaderStart();

        if (response.Success) {
          LoaderStop();
          if(response.Result.userType ==1){
            StorageUtils.set('role', 'admin');
            response.Result.isLogged = true;
            return UserSrv.set(response.Result).then(function () {
              $state.go('admin.dashboard');
            });
          }
          else {
            var errorMes= "invalid credentials";
            alert(errorMes);
          }


        } else {
          LoaderStop();
          var errorMes= response.Message;
          alert(errorMes);
        }
      });
    };

    function getRole(type){
      return (type == "1") ? 'admin' : (type == "2") ? 'users' : (type == "3") ? 'customers' : 'partners';
    }
  }
})();
