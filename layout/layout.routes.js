(function(){
  'use strict';
  angular.module('app')
    .config(configure);

  function configure($stateProvider){
    $stateProvider
      .state('loading', {
      url: '/loading',
      cache:false,
      templateUrl: 'layout/loading.html',
      controller: 'LoadingCtrl'
    })

    .state('admin', {
      url: '/admin',
      cache:false,
      abstract: true,
      templateUrl: 'layout/admin/homeLayout.html',
      controller: 'adminLayoutCtrl',
      data: {
        role:['admin'],
        restrictAccess: ['logged']
      }
    })

    // .state('users', {
    //   url: '/users',
    //   cache:false,
    //   abstract: true,
    //   templateUrl: 'layout/admin/homeLayout.html',
    //   controller: 'adminLayoutCtrl',
    //   data: {
    //     role:['users'],
    //     restrictAccess: ['logged']
    //   }
    // })
    //
    // .state('partners', {
    //    url: '/partners',
    //    cache:false,
    //    abstract: true,
    //    templateUrl: 'layout/Partners/homeLayout.html',
    //    controller: 'partnerLayoutCtrl',
    //    data: {
    //      role:['partners'],
    //      restrictAccess: ['logged']
    //    }
    //   })
    //
    // .state('facilities', {
    //    url: '/facilities',
    //    cache:false,
    //    abstract: true,
    //    templateUrl: 'layout/Facilities/facilityLayout.html',
    //    controller: 'facilityLayoutCtrl',
    //    data: {
    //      role:['facilities'],
    //      restrictAccess: ['logged']
    //    }
    //   })


  }
})();
