﻿(function () {
    'use strict';
    angular.module('app')
      .controller('adminLayoutCtrl', adminLayoutCtrl);

    function adminLayoutCtrl($scope, $state, AuthSrv, StorageUtils, $rootScope, aPIInterFace,UserSrv) {
        var vm = {};
        $scope.vm = vm;
        vm.currentUser = JSON.parse(localStorage.getItem('app-user'));
        vm.logout = logout;
        vm.currentpage=$state.current.diplayname;

          $rootScope.$on('$stateChangeStart', function (e, toState, currentState) {
             vm.currentpage=toState.diplayname;
           });

        function logout() {
            AuthSrv.logout().then(function () {
                UserSrv.delete();
                $state.go('login');
            });
        };

    }

})();
