'use strict';
(function () {


    function LoaderStart() {

        $('#loader').show();
    }


    function LoaderStop() {
        $('#loader').hide();
    }



    // container for data to be used for messages;
    var apiRoutes = {};
    window.apiRoutes = apiRoutes;
    var app = angular.module('app');

    app.run(['$rootScope', 'Config', function (rootScope, Config) {
        rootScope.apiRoutes = apiRoutes;
    }]);

    apiRoutes.routePath = Config.backendUrl;
    apiRoutes.routeAdminPath = Config.backendAdminUrl;

    apiRoutes.url =
    {
      // login :'TblUserLogin',

      'login': 'signup',
      'getcategories' : 'getcategories',
      'usersType' : 'getusers/:type',
      'usersAction' : 'action',
      'getadmincategories':'getadmincategories'

    };

    apiRoutes.getPath = function (key) {
        return apiRoutes.routePath + apiRoutes.url[key];
    }

    apiRoutes.getAdminPath = function (key) {
        return apiRoutes.routeAdminPath + apiRoutes.url[key];
    }

})(window.app);
